# Modelos_físicos_de_base_de_datos



## Planteamiento del caso a desarrollar

La tienda Rock Music es una empresa peruana líder en venta de Instrumentos musicales y Audio Profesional, y que ofrecer a sus clientes la mejor experiencia de compra, brindándoles asesoría antes, durante y después de su compra. Cuenta con más de 16 años en el mercado peruano ofreciendo variedad y garantía.

El catálogo que cuenta la tienda incluye: Guitarras, Percusión, Amplificadores, Pianos y Teclados, Pedales y Efectos, Home Studio, Audio Pro y Sonido en vivo, DJ, Arco y Viento y accesorios.

La tienda desea implementar un proyecto que mejore la información que almacena todo el stock de su catálogo de ventas, por lo que ha optado por el desarrollo de una base de datos que brinde de forma ágil la información que se cuenta para ponerla a la venta.

Por consiguiente, el proyecto consistirá en elaborar una base de datos, basada en Oracle, que permita cumplir con los requerimientos del cliente.
