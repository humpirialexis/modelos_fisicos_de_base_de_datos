-- Consultas
-- Recuperar todos los instrumentos
SELECT * FROM Instrumento;

-- Recuperar todos los clientes
SELECT * FROM Cliente;

SELECT * FROM DetallePedido

-- Recuperar todas las ventas y sus detalles
SELECT V.ID_Venta, V.Fecha, C.Nombre, C.Apellido, DV.ID_Instrumento, I.Nombre AS Instrumento, DV.Cantidad, DV.PrecioUnitario
FROM Venta V
JOIN Cliente C ON V.ID_Cliente = C.ID_Cliente
JOIN DetalleVenta DV ON V.ID_Venta = DV.ID_Venta
JOIN Instrumento I ON DV.ID_Instrumento = I.ID_Instrumento
ORDER BY V.Fecha;

-- Recuperar todos los pedidos y sus detalles
SELECT P.ID_Pedido, P.Fecha, PR.Nombre AS Proveedor, DP.ID_Instrumento, I.Nombre AS Instrumento, DP.Cantidad, DP.PrecioUnitario
FROM Pedido P
JOIN Proveedor PR ON P.ID_Proveedor = PR.ID_Proveedor
JOIN DetallePedido DP ON P.ID_Pedido = DP.ID_Pedido
JOIN Instrumento I ON DP.ID_Instrumento = I.ID_Instrumento
ORDER BY P.Fecha;
