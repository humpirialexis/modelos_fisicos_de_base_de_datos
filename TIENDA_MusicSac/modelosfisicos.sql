create database TIENDA_MusicSac

use TIENDA_MusicSac

CREATE TABLE Instrumento (
    ID_Instrumento int PRIMARY KEY,
    Nombre VARCHAR(50),
    Categoria VARCHAR(50),
    Precio decimal(10,2),
    Marca VARCHAR(50),
    Descripcion VARCHAR(100),
    Stock int
);

drop table Instrumento
drop table DetalleVenta

CREATE TABLE Cliente (
    ID_Cliente int PRIMARY KEY,
    Nombre VARCHAR(100),
    Apellido VARCHAR(100),
    Direccion VARCHAR(255),
    Telefono VARCHAR(15),
    Email VARCHAR(100)
);

CREATE TABLE Venta (
    ID_Venta int PRIMARY KEY,
    Fecha DATE,
    ID_Cliente int,
    FOREIGN KEY (ID_Cliente) REFERENCES Cliente(ID_Cliente)
);

CREATE TABLE DetalleVenta (
    ID_Detalle int PRIMARY KEY,
    ID_Venta int,
    ID_Instrumento int,
    Cantidad int,
    PrecioUnitario decimal(10,2),
    FOREIGN KEY (ID_Venta) REFERENCES Venta(ID_Venta),
    FOREIGN KEY (ID_Instrumento) REFERENCES Instrumento(ID_Instrumento)
);

CREATE TABLE Proveedor (
    ID_Proveedor int PRIMARY KEY,
    Nombre VARCHAR(100),
    Direccion VARCHAR(255),
    Telefono VARCHAR(15),
    Email VARCHAR(100)
);

CREATE TABLE Pedido (
    ID_Pedido int PRIMARY KEY,
    Fecha DATE,
    ID_Proveedor int,
    FOREIGN KEY (ID_Proveedor) REFERENCES Proveedor(ID_Proveedor)
);

CREATE TABLE DetallePedido (
    ID_Detalle int PRIMARY KEY,
    ID_Pedido int,
    ID_Instrumento int,
    Cantidad int,
    PrecioUnitario decimal(10,2),
    FOREIGN KEY (ID_Pedido) REFERENCES Pedido(ID_Pedido),
    FOREIGN KEY (ID_Instrumento) REFERENCES Instrumento(ID_Instrumento)
);


INSERT INTO Instrumento (ID_Instrumento, Nombre, Categoria, Precio, Marca, Descripcion, Stock)
VALUES
(1, 'Guitarra eléctrica Fender Stratocaster', 'Guitarras', 1500.00, 'Fender', 'Guitarra eléctrica de cuerpo sólido', 10),
(2, 'Batería acústica Pearl Export Series', 'Percusión', 2500.00, 'Pearl', 'Batería acústica profesional de 5 piezas', 5),
(3, 'Amplificador Marshall JVM410H', 'Amplificadores', 2000.00, 'Marshall', 'Cabezal de amplificador para guitarra eléctrica', 8),
(4, 'Piano digital Yamaha P-125', 'Pianos y Teclados', 1200.00, 'Yamaha', 'Piano digital con teclas contrapesadas', 7),
(5, 'Pedal de efecto Boss DS-1', 'Pedales y Efectos', 100.00, 'Boss', 'Pedal de distorsión para guitarra eléctrica', 15),
(6, 'Interfaz de audio Focusrite Scarlett 2i2', 'Home Studio', 300.00, 'Focusrite', 'Interfaz de audio USB para grabación profesional', 12),
(7, 'Altavoz activo JBL EON615', 'Audio Pro y Sonido en vivo', 700.00, 'JBL', 'Altavoz activo para aplicaciones de sonido en vivo', 20),
(8, 'Plato giradiscos Technics SL-1200MK7', 'DJ', 1500.00, 'Technics', 'Plato giradiscos profesional para DJ', 6),
(9, 'Flauta travesera Yamaha YFL-222', 'Arco y Viento', 600.00, 'Yamaha', 'Flauta travesera para estudiantes', 10),
(10, 'Cable para instrumento Mogami Gold', 'Accesorios', 50.00, 'Mogami', 'Cable de alta calidad para conexión de instrumentos', 30);


INSERT INTO Cliente (ID_Cliente, Nombre, Apellido, Direccion, Telefono, Email)
VALUES
(1, 'Juan', 'Perez', 'Av. Primavera 123, Lima', '987654321', 'juan.perez@example.com'),
(2, 'María', 'Gomez', 'Jr. Las Flores 456, Arequipa', '955432100', 'maria.gomez@example.com'),
(3, 'Carlos', 'López', 'Calle Los Pinos 789, Trujillo', '943210987', 'carlos.lopez@example.com'),
(4, 'Ana', 'Martinez', 'Av. La Paz 567, Cusco', '978654321', 'ana.martinez@example.com'),
(5, 'Pedro', 'García', 'Jr. Huancavelica 890, Huancayo', '965432100', 'pedro.garcia@example.com'),
(6, 'Laura', 'Diaz', 'Calle Los Claveles 123, Chiclayo', '917654321', 'laura.diaz@example.com'),
(7, 'Miguel', 'Rodriguez', 'Av. Los Olivos 456, Piura', '935432100', 'miguel.rodriguez@example.com'),
(8, 'Luisa', 'Sanchez', 'Jr. Los Jazmines 789, Ica', '987654322', 'luisa.sanchez@example.com'),
(9, 'Jorge', 'Torres', 'Calle Las Palmeras 567, Tacna', '955432101', 'jorge.torres@example.com'),
(10, 'Rosa', 'Fernandez', 'Av. Los Girasoles 890, Pucallpa', '943210988', 'rosa.fernandez@example.com');


-- Venta 1
INSERT INTO Venta (ID_Venta, Fecha, ID_Cliente)
VALUES
(1, '2024-06-01', 1);

-- DetalleVenta para Venta 1
INSERT INTO DetalleVenta (ID_Detalle, ID_Venta, ID_Instrumento, Cantidad, PrecioUnitario)
VALUES
(1, 1, 1, 1, 1500.00),
(2, 1, 5, 2, 100.00);

-- Venta 2
INSERT INTO Venta (ID_Venta, Fecha, ID_Cliente)
VALUES
(2, '2024-06-05', 3);

-- DetalleVenta para Venta 2
INSERT INTO DetalleVenta (ID_Detalle, ID_Venta, ID_Instrumento, Cantidad, PrecioUnitario)
VALUES
(3, 2, 4, 1, 1200.00),
(4, 2, 9, 1, 600.00);

-- Venta 3
INSERT INTO Venta (ID_Venta, Fecha, ID_Cliente)
VALUES
(3, '2024-06-10', 5);

-- DetalleVenta para Venta 3
INSERT INTO DetalleVenta (ID_Detalle, ID_Venta, ID_Instrumento, Cantidad, PrecioUnitario)
VALUES
(5, 3, 7, 1, 700.00),
(6, 3, 10, 3, 50.00);


INSERT INTO Proveedor (ID_Proveedor, Nombre, Direccion, Telefono, Email)
VALUES
(1, 'Distribuidora Musical SAC', 'Av. Industrial 234, Lima', '999876543', 'ventas@distribuidora.com'),
(2, 'Instruments & Co.', 'Jr. Comercio 567, Arequipa', '988765432', 'info@instrumentsco.com'),
(3, 'AudioTech SRL', 'Calle Sonido 890, Trujillo', '977654321', 'contacto@auditech.com'),
(4, 'Musical Producers EIRL', 'Av. Melodía 123, Cusco', '966543210', 'ventas@musicalproducers.com'),
(5, 'Instrumentos del Sur SAC', 'Jr. Rítmica 456, Chiclayo', '955432109', 'info@instrumentosdelsur.com'),
(6, 'DJ Equipments EIRL', 'Av. Remix 789, Piura', '944321098', 'ventas@djequipments.com'),
(7, 'SoundMasters SAC', 'Calle Sonido Vivo 012, Ica', '933210987', 'contacto@soundmasters.com'),
(8, 'Vientos del Norte EIRL', 'Jr. Flauta 345, Tacna', '922109876', 'ventas@vientosdelnorte.com'),
(9, 'Music Accessories SAC', 'Av. Accesorios 678, Pucallpa', '911098765', 'info@musicaccessories.com'),
(10, 'Audio Sound EIRL', 'Calle Estudio 901, Lima', '900987654', 'ventas@audiosound.com');


-- Pedido 1
INSERT INTO Pedido (ID_Pedido, Fecha, ID_Proveedor)
VALUES
(1, '2024-05-15', 1);

-- DetallePedido para Pedido 1
INSERT INTO DetallePedido (ID_Detalle, ID_Pedido, ID_Instrumento, Cantidad, PrecioUnitario)
VALUES
(1, 1, 1, 5, 1400.00),
(2, 1, 2, 2, 2300.00);

-- Pedido 2
INSERT INTO Pedido (ID_Pedido, Fecha, ID_Proveedor)
VALUES
(2, '2024-05-20', 3);

-- DetallePedido para Pedido 2
INSERT INTO DetallePedido (ID_Detalle, ID_Pedido, ID_Instrumento, Cantidad, PrecioUnitario)
VALUES
(3, 2, 6, 3, 270.00),
(4, 2, 8, 1, 1400.00);

-- Pedido 3
INSERT INTO Pedido (ID_Pedido, Fecha, ID_Proveedor)
VALUES
(3, '2024-05-25', 5);

SELECT * FROM Instrumento;
SELECT * FROM Cliente;
SELECT * FROM Venta;
SELECT * FROM DetalleVenta;
SELECT * FROM Proveedor;
SELECT * FROM Pedido;
SELECT * FROM DetallePedido;
