USE [TIENDA_MusicSac]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[ID_Cliente] [int] NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Apellido] [varchar](100) NULL,
	[Direccion] [varchar](255) NULL,
	[Telefono] [varchar](15) NULL,
	[Email] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePedido]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePedido](
	[ID_Detalle] [int] NOT NULL,
	[ID_Pedido] [int] NULL,
	[ID_Instrumento] [int] NULL,
	[Cantidad] [int] NULL,
	[PrecioUnitario] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleVenta]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleVenta](
	[ID_Detalle] [int] NOT NULL,
	[ID_Venta] [int] NULL,
	[ID_Instrumento] [int] NULL,
	[Cantidad] [int] NULL,
	[PrecioUnitario] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Detalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Instrumento]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Instrumento](
	[ID_Instrumento] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Categoria] [varchar](50) NULL,
	[Precio] [decimal](10, 2) NULL,
	[Marca] [varchar](50) NULL,
	[Descripcion] [varchar](100) NULL,
	[Stock] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Instrumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedido]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedido](
	[ID_Pedido] [int] NOT NULL,
	[Fecha] [date] NULL,
	[ID_Proveedor] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[ID_Proveedor] [int] NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Direccion] [varchar](255) NULL,
	[Telefono] [varchar](15) NULL,
	[Email] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Proveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 30/06/2024 16:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[ID_Venta] [int] NOT NULL,
	[Fecha] [date] NULL,
	[ID_Cliente] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (1, N'Juan', N'Perez', N'Av. Primavera 123, Lima', N'987654321', N'juan.perez@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (2, N'María', N'Gomez', N'Jr. Las Flores 456, Arequipa', N'955432100', N'maria.gomez@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (3, N'Carlos', N'López', N'Calle Los Pinos 789, Trujillo', N'943210987', N'carlos.lopez@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (4, N'Ana', N'Martinez', N'Av. La Paz 567, Cusco', N'978654321', N'ana.martinez@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (5, N'Pedro', N'García', N'Jr. Huancavelica 890, Huancayo', N'965432100', N'pedro.garcia@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (6, N'Laura', N'Diaz', N'Calle Los Claveles 123, Chiclayo', N'917654321', N'laura.diaz@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (7, N'Miguel', N'Rodriguez', N'Av. Los Olivos 456, Piura', N'935432100', N'miguel.rodriguez@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (8, N'Luisa', N'Sanchez', N'Jr. Los Jazmines 789, Ica', N'987654322', N'luisa.sanchez@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (9, N'Jorge', N'Torres', N'Calle Las Palmeras 567, Tacna', N'955432101', N'jorge.torres@example.com')
GO
INSERT [dbo].[Cliente] ([ID_Cliente], [Nombre], [Apellido], [Direccion], [Telefono], [Email]) VALUES (10, N'Rosa', N'Fernandez', N'Av. Los Girasoles 890, Pucallpa', N'943210988', N'rosa.fernandez@example.com')
GO
INSERT [dbo].[DetallePedido] ([ID_Detalle], [ID_Pedido], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (1, 1, 1, 5, CAST(1400.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetallePedido] ([ID_Detalle], [ID_Pedido], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (2, 1, 2, 2, CAST(2300.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetallePedido] ([ID_Detalle], [ID_Pedido], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (3, 2, 6, 3, CAST(270.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetallePedido] ([ID_Detalle], [ID_Pedido], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (4, 2, 8, 1, CAST(1400.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetallePedido] ([ID_Detalle], [ID_Pedido], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (5, 3, 3, 4, CAST(1800.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetallePedido] ([ID_Detalle], [ID_Pedido], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (6, 3, 7, 2, CAST(1300.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetalleVenta] ([ID_Detalle], [ID_Venta], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (1, 1, 1, 1, CAST(1500.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetalleVenta] ([ID_Detalle], [ID_Venta], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (2, 1, 5, 2, CAST(100.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetalleVenta] ([ID_Detalle], [ID_Venta], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (3, 2, 4, 1, CAST(1200.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetalleVenta] ([ID_Detalle], [ID_Venta], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (4, 2, 9, 1, CAST(600.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetalleVenta] ([ID_Detalle], [ID_Venta], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (5, 3, 7, 1, CAST(700.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[DetalleVenta] ([ID_Detalle], [ID_Venta], [ID_Instrumento], [Cantidad], [PrecioUnitario]) VALUES (6, 3, 10, 3, CAST(50.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (1, N'Guitarra eléctrica Fender Stratocaster', N'Guitarras', CAST(1500.00 AS Decimal(10, 2)), N'Fender', N'Guitarra eléctrica de cuerpo sólido', 10)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (2, N'Batería acústica Pearl Export Series', N'Percusión', CAST(2500.00 AS Decimal(10, 2)), N'Pearl', N'Batería acústica profesional de 5 piezas', 5)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (3, N'Amplificador Marshall JVM410H', N'Amplificadores', CAST(2000.00 AS Decimal(10, 2)), N'Marshall', N'Cabezal de amplificador para guitarra eléctrica', 8)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (4, N'Piano digital Yamaha P-125', N'Pianos y Teclados', CAST(1200.00 AS Decimal(10, 2)), N'Yamaha', N'Piano digital con teclas contrapesadas', 7)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (5, N'Pedal de efecto Boss DS-1', N'Pedales y Efectos', CAST(100.00 AS Decimal(10, 2)), N'Boss', N'Pedal de distorsión para guitarra eléctrica', 15)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (6, N'Interfaz de audio Focusrite Scarlett 2i2', N'Home Studio', CAST(300.00 AS Decimal(10, 2)), N'Focusrite', N'Interfaz de audio USB para grabación profesional', 12)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (7, N'Altavoz activo JBL EON615', N'Audio Pro y Sonido en vivo', CAST(700.00 AS Decimal(10, 2)), N'JBL', N'Altavoz activo para aplicaciones de sonido en vivo', 20)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (8, N'Plato giradiscos Technics SL-1200MK7', N'DJ', CAST(1500.00 AS Decimal(10, 2)), N'Technics', N'Plato giradiscos profesional para DJ', 6)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (9, N'Flauta travesera Yamaha YFL-222', N'Arco y Viento', CAST(600.00 AS Decimal(10, 2)), N'Yamaha', N'Flauta travesera para estudiantes', 10)
GO
INSERT [dbo].[Instrumento] ([ID_Instrumento], [Nombre], [Categoria], [Precio], [Marca], [Descripcion], [Stock]) VALUES (10, N'Cable para instrumento Mogami Gold', N'Accesorios', CAST(50.00 AS Decimal(10, 2)), N'Mogami', N'Cable de alta calidad para conexión de instrumentos', 30)
GO
INSERT [dbo].[Pedido] ([ID_Pedido], [Fecha], [ID_Proveedor]) VALUES (1, CAST(N'2024-05-15' AS Date), 1)
GO
INSERT [dbo].[Pedido] ([ID_Pedido], [Fecha], [ID_Proveedor]) VALUES (2, CAST(N'2024-05-20' AS Date), 3)
GO
INSERT [dbo].[Pedido] ([ID_Pedido], [Fecha], [ID_Proveedor]) VALUES (3, CAST(N'2024-05-25' AS Date), 5)
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (1, N'Distribuidora Musical SAC', N'Av. Industrial 234, Lima', N'999876543', N'ventas@distribuidora.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (2, N'Instruments & Co.', N'Jr. Comercio 567, Arequipa', N'988765432', N'info@instrumentsco.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (3, N'AudioTech SRL', N'Calle Sonido 890, Trujillo', N'977654321', N'contacto@auditech.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (4, N'Musical Producers EIRL', N'Av. Melodía 123, Cusco', N'966543210', N'ventas@musicalproducers.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (5, N'Instrumentos del Sur SAC', N'Jr. Rítmica 456, Chiclayo', N'955432109', N'info@instrumentosdelsur.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (6, N'DJ Equipments EIRL', N'Av. Remix 789, Piura', N'944321098', N'ventas@djequipments.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (7, N'SoundMasters SAC', N'Calle Sonido Vivo 012, Ica', N'933210987', N'contacto@soundmasters.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (8, N'Vientos del Norte EIRL', N'Jr. Flauta 345, Tacna', N'922109876', N'ventas@vientosdelnorte.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (9, N'Music Accessories SAC', N'Av. Accesorios 678, Pucallpa', N'911098765', N'info@musicaccessories.com')
GO
INSERT [dbo].[Proveedor] ([ID_Proveedor], [Nombre], [Direccion], [Telefono], [Email]) VALUES (10, N'Audio Sound EIRL', N'Calle Estudio 901, Lima', N'900987654', N'ventas@audiosound.com')
GO
INSERT [dbo].[Venta] ([ID_Venta], [Fecha], [ID_Cliente]) VALUES (1, CAST(N'2024-06-01' AS Date), 1)
GO
INSERT [dbo].[Venta] ([ID_Venta], [Fecha], [ID_Cliente]) VALUES (2, CAST(N'2024-06-05' AS Date), 3)
GO
INSERT [dbo].[Venta] ([ID_Venta], [Fecha], [ID_Cliente]) VALUES (3, CAST(N'2024-06-10' AS Date), 5)
GO
ALTER TABLE [dbo].[DetallePedido]  WITH CHECK ADD FOREIGN KEY([ID_Instrumento])
REFERENCES [dbo].[Instrumento] ([ID_Instrumento])
GO
ALTER TABLE [dbo].[DetallePedido]  WITH CHECK ADD FOREIGN KEY([ID_Pedido])
REFERENCES [dbo].[Pedido] ([ID_Pedido])
GO
ALTER TABLE [dbo].[DetalleVenta]  WITH CHECK ADD FOREIGN KEY([ID_Instrumento])
REFERENCES [dbo].[Instrumento] ([ID_Instrumento])
GO
ALTER TABLE [dbo].[DetalleVenta]  WITH CHECK ADD FOREIGN KEY([ID_Venta])
REFERENCES [dbo].[Venta] ([ID_Venta])
GO
ALTER TABLE [dbo].[Pedido]  WITH CHECK ADD FOREIGN KEY([ID_Proveedor])
REFERENCES [dbo].[Proveedor] ([ID_Proveedor])
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD FOREIGN KEY([ID_Cliente])
REFERENCES [dbo].[Cliente] ([ID_Cliente])
GO
